import tinycss
import logging
import logging.handlers
from workflowCSS.processor import *
from workflowCSS.convertor import generate_xml_workflow, generate_picture
from lxml.etree import tostring
from itertools import chain
import argparse
import os


def main():
    parser = argparse.ArgumentParser(description='Help to convert workflowCSS file to xml')
    parser.add_argument('file', metavar='file', type=str, help='workflowCSS file')
    parser.add_argument('--output', type=str, help='output file name', default=None)
    parser.add_argument('--create-image', '-i', help='Add svg image to output', action='store_true')
    parser.add_argument('--disable-workflow', '-d', help='Don\'t generate workflow', action='store_true')
    parser.add_argument('--log', help='Add log output', action='store_true')

    args = parser.parse_args()

    if args.output is None:
        args.output = os.path.splitext(os.path.basename(args.file))[0]

    _log = logging.getLogger('WorkflowCSS parser')
    ch = logging.StreamHandler()
    _log.addHandler(ch)
    if args.log:
        _log.setLevel(logging.INFO)
    else:
        _log.setLevel(logging.WARNING)
    parser = tinycss.CSS21Parser()
    if not os.path.exists(args.file):
        raise Exception('File %s not exists' % args.file)
    stylesheet = parser.parse_stylesheet_file(args.file)
    rules = stylesheet.rules

    for error in stylesheet.errors:
        _log.error(error)

    workflow_rule = extract_workflow_rule(rules)
    activities_rules = extract_activity_rule(rules)
    activity_templates_rules = extract_activity_template_rule(rules)
    short_activities_rules = extract_short_activities(rules)
    transition_rules = set(stylesheet.rules).difference(workflow_rule).difference(activities_rules).difference(
        activity_templates_rules).difference(short_activities_rules)

    _log.info('Hello, and welcome to another parser task!')
    # Process activities
    activities_df = pd.DataFrame(map(process_activity, activities_rules))
    _log.info('Initial activities')
    _log.info('\n' + activities_df.to_string())
    result = list(chain.from_iterable(map(process_short_activity, short_activities_rules)))
    activities_df = activities_df.append(result, ignore_index=True)
    _log.info('With short activities')
    _log.info('\n' + activities_df.to_string())
    activity_template_identifications = dict()
    for activity_template_rule in activity_templates_rules:
        activities_df, new_activities_names = process_activity_template(activity_template_rule, activities_df)
        activity_template_identifications[activity_template_rule.selector[0].value] = new_activities_names
    if 'source' in activities_df.columns:
        activities_df = activities_df.drop('source', 1)
    _log.info('With template activities')
    _log.info('\n' + activities_df.to_string())
    raw_transitions_df = pd.DataFrame(map(process_raw_transition, transition_rules))
    _log.info('Raw transitions')
    _log.info('\n' + raw_transitions_df.to_string())
    half_raw_transitions_df = process_big_transitions(raw_transitions_df, activity_template_identifications)
    _log.info('Half transitions')
    _log.info('\n' + half_raw_transitions_df.to_string())
    processed_transitions_df = process_extend_transitions(half_raw_transitions_df, activity_template_identifications)
    _log.info('Processed transitions')
    _log.info('\n' + processed_transitions_df.to_string())
    transitions_df = convert_transitions(processed_transitions_df)
    _log.info('Transitions')
    _log.info('\n' + transitions_df.to_string())
    if not args.disable_workflow:
        workflow_xml = generate_xml_workflow(activities_df, transitions_df, workflow_rule[0])
        workflow = open(args.output + '.xml', 'w')
        workflow.write(tostring(workflow_xml, pretty_print=True, encoding='utf-8', xml_declaration=True)
                       .replace('</record>', '</record>\n'))
        workflow.close()
    if args.create_image:
        print 'Generate image'
        generate_picture(activities_df, transitions_df, path=args.output + '.svg')


if __name__ == "__main__":
    main()
