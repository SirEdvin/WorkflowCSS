from setuptools import setup

setup(
    name='workflowCSS',
    version='0.45',
    url='',
    license='',
    author='siredvin',
    author_email='',
    description='',
    install_requires=[
          'tinycss', 'pandas', 'lxml', 'networkx', 'matplotlib', 'numpy'
    ],
    packages=['workflowCSS'],
    scripts=['bin/wcss2xml.py'],
    entry_points={
        'console_scripts': [
            'wcss2xml = wcss2xml:main']
    }
)
