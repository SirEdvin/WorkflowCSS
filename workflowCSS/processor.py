from tools import *
import numpy as np
import pandas as pd
from tinycss.token_data import Token, TokenList
from itertools import chain

TEMPLATE_ACTIVITY_TRANSFERRED_FIELDS = (
    'kind', 'action', 'flow_start', 'flow_stop', 'subflow_id', 'split_mode', 'join_mode')


def extract_workflow_rule(rules):
    """
    Extract workflow rule from rule list.
    Workflow rule has only 1 selector with 'workflow' name
    """
    return filter(lambda rule: len(rule.selector) == 1 and rule.selector[0].value == 'workflow', rules)


def extract_activity_rule(rules):
    """
    Extract activity rule from rule list.
    Workflow rule has only 1 selector. Deprecated names 'workflow', 'activity'
    """
    return filter(lambda rule: len(rule.selector) == 1 and rule.selector[0].value not in ('workflow', 'activity'),
                  rules)


def extract_activity_template_rule(rules):
    """
    Extract activity template rule from rule list.
    Workflow rule has 3 selector and last of them are 'template'
    """
    return filter(lambda rule: len(rule.selector) == 3 and rule.selector[2].value == 'template', rules)


def extract_short_activities(rules):
    """
    Extract short activity rules from rule list.
    Workflow rule has only 1 selector with 'activity' name
    """
    return filter(lambda rule: len(rule.selector) == 1 and rule.selector[0].value == 'activity', rules)


def process_activity(activity_rule):
    """
    Convert activity rule to activity dict
    """
    activity = {
        'name': activity_rule.selector[0].value,
        'priority': 1
    }
    activity.update(convert_rule_declarations_to_dict(activity_rule))
    return activity


def process_short_activity(short_activity_rule):
    """
    Convert short activity rule to activities list
    """
    short_activity_dict = convert_rule_declarations_to_dict(short_activity_rule)
    exported_dict = dict(
        (key, value) for key, value in short_activity_dict.iteritems() if key in TEMPLATE_ACTIVITY_TRANSFERRED_FIELDS)
    template = short_activity_dict['template']
    keys = map(lambda r: r.strip(), template.split('&'))
    activity_descriptions = dict(
        (key, value) for key, value in short_activity_dict.iteritems() if
        (key not in TEMPLATE_ACTIVITY_TRANSFERRED_FIELDS) and (key != 'template'))
    activity_list = []
    for key, packed_values in activity_descriptions.items():
        activity = {
            'name': key,
            'priority': 1
        }
        activity.update(exported_dict)
        values = map(lambda r: r.strip(), packed_values.split('&'))
        for index in xrange(len(keys)):
            if len(values) == index:
                break
            value = values[index]
            if len(value) == 0:
                value = np.nan
            activity[keys[index]] = value
        activity_list.append(activity)
    return activity_list


def process_activity_template(activity_template_rule, activities_df):
    """
    Convert activity template rule to activities list, used activity DataFrame
    """
    activity_template = convert_rule_declarations_to_dict(activity_template_rule)
    allowed_regex = update_regex(activity_template.get('allowed_regex', '.*'))
    # Filter activities with allowed regex
    if len(allowed_regex) > 0:
        allowed_activities = activities_df[activities_df.name.str.contains(allowed_regex)]
    else:
        allowed_activities = pd.DataFrame(columns=activities_df.columns.copy())
    allowed = activity_template.get('allowed', None)
    if allowed:
        allowed_activities = allowed_activities.append(activities_df[activities_df.name.isin(eval(allowed))]).drop_duplicates()
    banned_regex = update_regex(activity_template.get('banned_regex', None))
    if banned_regex:
        allowed_activities = allowed_activities[allowed_activities.str.contains(banned_regex).map(lambda b: not b)]
    banned = activity_template.get('banned', None)
    if banned:
        allowed_activities = allowed_activities[allowed_activities.name.isin(eval(banned)).map(lambda s: not s)]
    if not convert_bool_from_string(activity_template.get('subflow_allowed', 'true')):
        allowed_activities = allowed_activities[allowed_activities.kind != 'subflow']
    if not convert_bool_from_string(activity_template.get('flow_start_allowed', 'true')):
        allowed_activities = allowed_activities[allowed_activities.flow_start != 'true']
    if not convert_bool_from_string(activity_template.get('flow_stop_allowed', 'true')):
        allowed_activities = allowed_activities[allowed_activities.flow_stop != 'true']
    # Activity generation
    exported_dict = dict(
        (key, value) for key, value in activity_template.iteritems() if key in TEMPLATE_ACTIVITY_TRANSFERRED_FIELDS)
    new_activities_list = []
    for index, activity in allowed_activities.iterrows():
        name = activity_template['name_template'] % activity['name']
        new_activity = {
            'name': name,
            'source': activity['name'],
            'priority': 2
        }
        new_activity.update(exported_dict)
        new_activities_list.append(new_activity)
    if len(new_activities_list) == 0:
        return activities_df, {}
    return activities_df.append(new_activities_list, ignore_index=True), map(lambda r: {'name': r['name'], 'source': r['source']},
                                                          new_activities_list)


def process_raw_transition(transition_rule):
    """
    Convert transition rule to transition dict
    I have a lot of strange logic in this place
    :Short step processing: Process chain transition lines and convert short 'signal', 'condition' to 'step'
    :Step collection: Collect step to list
    """
    transition = {
        'selector': process_selector(transition_rule.selector),
        'priority': 1
    }
    if len(transition['selector']) > 2 and transition['selector'][1] != 'template':
        for declaration in transition_rule.declarations:
            if declaration.name in ('signal', 'condition'):
                converted_result = '%s -> ' % declaration.name
                declaration.name = 'step'
                declaration.value = TokenList(chain.from_iterable(
                    [[Token('IDENT', converted_result, converted_result, None, 0, 0)], [declaration.value]]))
    transition.update(convert_rule_declarations_to_dict(transition_rule))
    return transition


def process_big_transitions(raw_transitions_df, activity_template_identifications):
    """
    Process big transition
    Main goal of this method - remove all complex transitions and left transitions with 2 selectors only
    :Process transition types: template transition, chain transition
    """
    index_to_remove = []
    insert_list = []
    for index, selector in raw_transitions_df.selector.iteritems():
        if len(selector) > 2:
            index_to_remove.append(index)
            if selector[1] == 'template':
                template_activity = selector[0] if selector[0] != 'source' else selector[2]
                for info in activity_template_identifications[template_activity]:
                    transition = raw_transitions_df.iloc[index].copy()
                    transition['priority'] = 3
                    if selector[0] == 'source':
                        transition['selector'] = [info['source'], info['name']]
                    else:
                        transition['selector'] = [info['name'], info['source']]
                    insert_list.append(transition)
                pass
            else:
                prev_step = selector[0]
                steps = raw_transitions_df.iloc[index]['step']
                for step_index in xrange(0, len(steps)):
                    activity = selector[step_index + 1]
                    step_description = steps[step_index].split('&')
                    transition = raw_transitions_df.iloc[index].copy()
                    transition['selector'] = [prev_step, activity]
                    if len(step_description) > 0 and step_description[0] != 'None':
                        for current_step_description in step_description:
                            current_step_description_list = map(lambda r: r.strip(), current_step_description.split('->'))
                            transition[current_step_description_list[0]] = current_step_description_list[1]
                    transition['step'] = np.nan
                    prev_step = activity
                    insert_list.append(transition)
    if len(insert_list) > 0:
        half_raw_transitions_df = raw_transitions_df.drop(raw_transitions_df.index[index_to_remove])
        return half_raw_transitions_df.append(insert_list, ignore_index=True).drop('step', 1)
    else:
        return raw_transitions_df


def process_extend_transitions(raw_transitions_df, activity_template_identifications):
    """
    Process two-selector transitions, convert them to transition with simple selectors (only activities, no templates)
    """
    index_to_remove = []
    insert_list = []
    for index, selector in raw_transitions_df.selector.iteritems():
        if ('|' in selector[0]) or ('|' in selector[1]):
            index_to_remove.append(index)
            act_from_list = selector[0].split('|')
            act_to_list = selector[1].split('|')
            for act_from in act_from_list:
                for act_to in act_to_list:
                    transition = raw_transitions_df.iloc[index].copy()
                    transition['selector'] = [act_from, act_to]
                    insert_list.append(transition)
    if len(insert_list) > 0:
        processed_transitions_df = raw_transitions_df.drop(raw_transitions_df.index[index_to_remove])
        processed_transitions_df = processed_transitions_df.append(insert_list, ignore_index=True)
    else:
        processed_transitions_df = raw_transitions_df
    index_to_remove = []
    insert_list = []
    for index, selector in processed_transitions_df.selector.iteritems():
        if activity_template_identifications.get(selector[0]) or activity_template_identifications.get(selector[1]):
            index_to_remove.append(index)
            act_from_list = activity_template_identifications.get(selector[0], [selector[0]])
            act_to_list = activity_template_identifications.get(selector[1], [selector[1]])
            if len(act_from_list) > 1 and len(act_to_list) > 1:
                for act_from in act_from_list:
                    for act_to in act_to_list:
                        if act_from['source'] == act_to['source']:
                            transition = processed_transitions_df.iloc[index].copy()
                            transition['selector'] = [act_from['name'], act_to['name']]
                            transition['priority'] = 2
                            insert_list.append(transition)
            else:
                for act_from in act_from_list:
                    for act_to in act_to_list:
                        transition = processed_transitions_df.iloc[index].copy()
                        transition['selector'] = [act_from['name'], act_to['name']]
                        transition['priority'] = 2
                        insert_list.append(transition)
    if len(insert_list) > 0:
        processed_transitions_df = processed_transitions_df.drop(processed_transitions_df.index[index_to_remove])
        return processed_transitions_df.append(insert_list, ignore_index=True)
    else:
        return processed_transitions_df


def convert_transitions(transitions_df):
    """
    Split selector to act_from and act_to
    Process revert transition
    :return: pure transition df
    """
    temp_split_dataframe = pd.DataFrame(transitions_df.selector.apply(pd.Series))
    temp_split_dataframe.columns = ['act_from', 'act_to']
    transitions_df = transitions_df.drop('selector', 1)
    transitions_df['act_from'] = temp_split_dataframe.act_from
    transitions_df['act_to'] = temp_split_dataframe.act_to
    transitions_df = transitions_df.fillna(False)
    insert_list = []
    for index, row in transitions_df.iterrows():
        if row.get('revert_condition') or row.get('revert_signal'):
            reverse_row = row.copy()
            reverse_row['act_from'] = row['act_to']
            reverse_row['act_to'] = row['act_from']
            reverse_row['signal'] = row.get('revert_signal', np.nan)
            reverse_row['condition'] = row.get('revert_condition', np.nan)
            insert_list.append(reverse_row)
    if len(insert_list) > 0:
        transitions_df = transitions_df.append(insert_list, ignore_index=True)
    if 'revert_signal' in transitions_df.columns:
        transitions_df = transitions_df.drop('revert_signal', 1)
    if 'revert_condition' in transitions_df.columns:
        transitions_df = transitions_df.drop('revert_condition', 1)
    return transitions_df
