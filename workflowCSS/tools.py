#-*- coding:utf-8 -*-


def convert_bool_from_string(text):
    assert isinstance(text, str) or isinstance(text, unicode)
    text = text.lower()
    return text == 'true'


def update_regex(regex):
    if not regex:
        return regex
    if not regex.startswith('^'):
        regex = '^' + regex
    if not regex.endswith('$'):
        regex += '$'
    return regex


def process_selector(selector):
    result = []
    acc = ''
    for token in selector:
        if token.type in ('IDENT', 'DELIM'):
            acc += token.value
        else:
            result.append(acc)
            acc = ''
    result.append(acc)
    return result if len(result) > 1 else result[0]


def convert_rule_declarations_to_dict(rule):
    declaration_dict = dict()
    for declaration in rule.declarations:
        if declaration_dict.get(declaration.name):
            if not isinstance(declaration_dict[declaration.name], list):
                declaration_dict[declaration.name] = [declaration_dict[declaration.name]]
            declaration_dict[declaration.name].append(declaration.value.as_css().replace('\{', '{').replace('\}', '}'))
        else:
            declaration_dict[declaration.name] = declaration.value.as_css().replace('\{', '{').replace('\}', '}')
        if declaration_dict[declaration.name] == 'None':
            declaration_dict[declaration.name] = ''
    return declaration_dict


def short_id(id_text):
    if len(id_text) > 64:
        return '___'.join([id_text[:30], id_text[-30:]])
    else:
        return id_text
