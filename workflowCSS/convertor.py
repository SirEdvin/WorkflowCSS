# -*- coding:utf-8 -*-
from lxml import etree
from tools import convert_rule_declarations_to_dict, short_id
import networkx as nx
import numpy as np
from matplotlib.pyplot import figure, axis

TECHNICAL_FIELDS = ['priority']
TRANSITIONS_TEXT = ['Basic transitions', 'Between template activities transitions', 'Template transitions']
ACTIVITIES_TEXT = ['Basic activities', 'Template activities']


def generate_workflow(data, workflow_dict):
    """
    Generate workflow from workflow_rule
    :return: workflow_id
    """
    _id = workflow_dict['name'].replace('.', '_') + '_workflow'
    workflow = etree.SubElement(data, 'record', {
        'id': _id,
        'model': 'workflow'
    })
    etree.SubElement(workflow, 'field', {
        'name': 'on_create'
    }).text = 'true'
    generate_fields(workflow_dict, workflow)
    return _id


def generate_fields(row, activity, ref_list=[]):
    """
    Generate field for activity
    Process '$' submol (set fields attrib)
    :param ref_list: for fields in this list will be setted 'ref' field instead text
    """
    for key, value in row.iteritems():
        # Some field in data frame are technical, and must be skipped
        if key in TECHNICAL_FIELDS:
            continue
        if value and value != np.nan:
            attrib = {
                'name': key
            }
            text = value
            if (isinstance(value, str) or isinstance(value, unicode)) and '$' in value:  # Extract dict from value
                values = value.replace('$', '').split('&')
                values = [s.strip().split(':') for s in values if len(s) > 0]
                text = None
                for temp_value in values:
                    if len(temp_value) == 1:
                        text = temp_value[0]
                        continue
                    attrib[temp_value[0].strip()] = temp_value[1].strip()
            if key in ref_list:
                attrib['ref'] = text
                text = None
            etree.SubElement(activity, 'field', attrib).text = text


def generate_xml_workflow(activities_df, transitions_df, workflow_rule):
    """
    Generate workflow xml data for odoo
    :return: xml element
    """
    activities_df = activities_df.sort_values('name')
    transitions_df = transitions_df.sort_values(['act_to', 'act_from'])
    openerp = etree.Element('openerp')
    openerp.append(etree.Comment('Autogenerated from WorkflowCSS'))
    activities_data = etree.SubElement(openerp, 'data')
    trasitions_data = etree.SubElement(openerp, 'data')
    # Generate workflow
    workflow_dict = convert_rule_declarations_to_dict(workflow_rule)
    workflow_id = generate_workflow(activities_data, workflow_dict)
    _pf = ''.join([s[0] for s in workflow_dict['name'].split('.')])
    # Activites processing
    activities_df = activities_df.fillna(False).sort_values('priority')
    activities_ids = {}
    last_priority = 1
    activities_data.append(etree.Comment(ACTIVITIES_TEXT[last_priority-1]))
    for index, row in activities_df.iterrows():
        if last_priority != row['priority']:
            activities_data.append(etree.Comment(ACTIVITIES_TEXT[row['priority']-1]))
        last_priority = row['priority']
        id = '_'.join(['act', row['name'], _pf])
        activities_ids[row['name']] = id
        activity = etree.SubElement(activities_data, 'record', {
            'id': short_id(id),
            'model': 'workflow.activity'
        })
        etree.SubElement(activity, 'field', {
            'name': 'wkf_id',
            'ref': workflow_id
        })
        generate_fields(row, activity)
    # Trasitions processing
    transitions_df = transitions_df.fillna(False).sort_values('priority')
    transitions_df.act_from = transitions_df.act_from.map(lambda r: activities_ids[r])
    transitions_df.act_to = transitions_df.act_to.map(lambda r: activities_ids[r])
    last_priority = 1
    trasitions_data.append(etree.Comment(TRANSITIONS_TEXT[last_priority-1]))
    for index, row in transitions_df.iterrows():
        if last_priority != row['priority']:
            trasitions_data.append(etree.Comment(TRANSITIONS_TEXT[row['priority']-1]))
        last_priority = row['priority']
        id = 'trasn_%s_%s' % (row['act_from'], row['act_to'])
        transition = etree.SubElement(trasitions_data, 'record', {
            'id': short_id(id),
            'model': 'workflow.transition'
        })
        generate_fields(row, transition, ref_list=['act_to', 'act_from'])
    return openerp


def generate_picture(activities_df, transitions_df, path='workflow.svg'):
    """
    Generate picture for workflow
    :param path: Path to save picture
    """
    activities_df = activities_df.fillna(False)
    transitions_df = transitions_df.fillna(False)
    workflow = nx.DiGraph()
    workflow.add_nodes_from(activities_df.name)
    for index, row in transitions_df.iterrows():
        workflow.add_edge(row['act_from'], row['act_to'], weight=20, signal=row['signal'], condition=row['condition'])
    pos=nx.graphviz_layout(workflow)
    f1 = figure(figsize=(100, 100))
    nx.draw_networkx_nodes(workflow, pos=pos, node_size=20000, node_shape='D', node_color='w')
    nx.draw_networkx_labels(workflow, pos=pos, font_size=8)
    nx.draw_networkx_edges(workflow, pos=pos)
    generated_labels = dict()
    for edge in workflow.edges_iter():
        revert_tuple = (edge[1], edge[0])
        signal = workflow[edge[0]][edge[1]]['signal']
        condition = workflow[edge[0]][edge[1]]['condition']
        label = edge[0][:3] + '-->' + edge[1][:3]
        if signal and signal != 'False':
            label += '\n'
            label += 'signal: %s' % signal
        if condition and condition != 'False':
            label += '\n'
            label += 'condition: %s' % condition
        if generated_labels.get(revert_tuple):
            generated_labels[revert_tuple] += '\n' + label
        else:
            generated_labels.update({edge: label})
    nx.draw_networkx_edge_labels(workflow, pos=pos, edge_labels=generated_labels)
    axis('off')
    f1.savefig(path)
